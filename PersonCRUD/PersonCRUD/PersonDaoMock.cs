﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonCRUD
{
    class PersonDaoMock : IPersonDao
    {
        List<Person> personList;

        public PersonDaoMock()
        {
            personList = new List<Person>();
            personList.Add(new Person(1, "Lebron", "James", 32));
            personList.Add(new Person(2, "Tony", "Parker", 35));
            personList.Add(new Person(3, "Kobe", "Bryant", 35));
            personList.Add(new Person(4, "Lamar", "Odom", 24));
            personList.Add(new Person(5, "Chris", "Paul", 22));
            personList.Add(new Person(6, "Tim", "Duncan", 38));
            personList.Add(new Person(7, "Kevin", "Garnet", 28));
            personList.Add(new Person(8, "Kawai", "Leonard", 24));
            personList.Add(new Person(9, "Ray", "Allen", 33));
            personList.Add(new Person(10, "Tracy", "McGrady", 32));
        }
        public void Create(Person p)
        {
            personList.Add(p);
        }

        public List<Person> Read()
        {
            return personList;
        }

        public void Update(Person p)
        {
            foreach (var item in personList)
            {
                if (item.Id == p.Id)
                {
                    item.FirstName = p.FirstName;
                    item.LastName = p.LastName;
                    item.Age = p.Age;
                    break;
                }
            }
        }

        public void Delete(Person p)
        {
            foreach (var item in personList)
            {
                if (item.Id == p.Id)
                {
                    personList.Remove(item);
                    break;
                }
            }
        }

    }
}
