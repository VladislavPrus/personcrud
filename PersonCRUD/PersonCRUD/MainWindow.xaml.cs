﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PersonCRUD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TableModel tm;
        public MainWindow()
        {

            InitializeComponent();
            tm = new TableModel();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            tm.Create(ReadPerson());
            SetDataGrid();
        }

        private void btnRead_Click(object sender, RoutedEventArgs e)
        {
            SetDataGrid();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            tm.Update(ReadPerson());
            SetDataGrid();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            tm.Delete(ReadPerson());
            SetDataGrid();
        }

        private Person ReadPerson()
        {
            int id = Convert.ToInt32(tbId.Text);
            string fname = tbFirstName.Text;
            string lname = tbLastName.Text;
            int age = Convert.ToInt32(tbAge.Text);
            return new Person(id, fname, lname, age);
        }

        private void SetDataGrid()
        {
            personGrid.ItemsSource = null;
            personGrid.ItemsSource = tm.Read();
        }
    }
}
