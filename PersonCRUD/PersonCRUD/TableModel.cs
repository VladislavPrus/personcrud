﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonCRUD
{
    class TableModel
    {
        IPersonDao db;
        public TableModel()
        {
            db = new PersonDaoMock();
        }

        public void Create(Person p)
        {
            db.Create(p);
        }
        public List<Person> Read()
        {
            return db.Read();
        }
        public void Update(Person p)
        {
            db.Update(p);
        }
        public void Delete(Person p)
        {
            db.Delete(p);
        }

    }
}
